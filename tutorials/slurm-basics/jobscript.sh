#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH --time=10:00:00
srun job_script.sh
root@schlaubi:/media/compute/vol/tutorial/slurm-basics# cat jobscript.sh 
#!/bin/bash
module load cuda/9.0
CUDA_DEVICE=$(echo "$CUDA_VISIBLE_DEVICES," | cut -d',' -f $((SLURM_LOCALID + 1)) );
T_REGEX='^[0-9]$';
if ! [[ "$CUDA_DEVICE" =~ $T_REGEX ]]; then
        echo "error no reserved gpu provided"
        exit 1;
fi
LOCAL_STORAGE="/media/compute/local/provisioned/datadir-${SLURM_JOB_UID}_${SLURM_JOB_ID}";
[ -d $LOCAL_STORAGE ] && { echo "i may use $LOCAL_STORAGE for temporary work data"; }
echo "Process $SLURM_PROCID of Job $SLURM_JOBID withe the local id $SLURM_LOCALID using gpu id $CUDA_DEVICE (we may use gpu: $CUDA_VISIBLE_DEVICES on $(hostname))"
echo "computing on $(nvidia-smi --query-gpu=gpu_name --format=csv -i $CUDA_DEVICE | tail -n 1)"
sleep 15
echo "done"
