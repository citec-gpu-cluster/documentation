# CIT-EC GPU Cluster

For detailed documentation of the GPU cluster see the Documentation [Pages](https://citec-gpu-cluster.pages.ub.uni-bielefeld.de/documentation/) 

## Support Channel

On the [Universities Matrix Service](https://matrix.uni-bielefeld.de/) join the Channel `#citec-gpu:uni-bielefeld.de`

## Developement

This site is built with Hugo.

To see your changes, [install Hugo](https://gohugo.io/getting-started/installing), then run it with:

```sh
hugo server -F -s docs -d ../public
```

Contributions welcome.

### Contributing

1. [Fork this repository](https://gitlab.ub.uni-bielefeld.de/citec-gpu-cluster/documentation/-/forks/new)
2. Create your feature branch (git checkout -b feature/fooBar)
3. Commit your changes (git commit -am 'Add some fooBar')
4. Push to the branch (git push origin feature/fooBar)
5. Create a new Pull Request
