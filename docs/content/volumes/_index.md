---
title: Volumes
date: 2023-10-24T15:12:06+02:00
weight: 15
chapter: true
pre: <b>3. </b>
---

# Volumes

On the cluster, you have access to several software volumes as well data sets. A list of existing volumes is in the following tables:


### Software volumes

| Name     | Path                   |
|----------|------------------------|
|Drake     | /vol/drake/            |
|Matlab    | /vol/matlab/bin/matlab |
|Mujoco    | /vol/mujoco/           |
|Openface  | /vol/openface/         |

Further volumes can be created. Please write a mail to [GPU Support](mailto:gpu-support@techfak.de). Please, provide the name, the quota of the volume, and optionally the group to set the read/write permissions.