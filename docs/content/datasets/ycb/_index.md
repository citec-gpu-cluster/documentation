---
title: "YCB"
weight: 50
hidden: true
description: 6D poses of known objects
---

# YCB-V (YCB-Video) BOP (Benchmark for 6D Object Pose Estimation)

- Path: `/media/compute/vol/ycb/ycb-bop`
- size: ~300GB?
- license: MIT

Xiang et al.: PoseCNN: A Convolutional Neural Network for 6D Object Pose Estimation in Cluttered Scenes, RSS 2018, [project website](https://rse-lab.cs.washington.edu/projects/posecnn/)

21 [YCB objects](https://www.ycbbenchmarks.com/) captured in 92 videos. Compared to the [original YCB-Video dataset](https://rse-lab.cs.washington.edu/projects/posecnn/), the differences in the BOP version are:

- Only a subset of test images is used for the evaluation - a subset of 75 images was manually selected for each of the 12 test scenes to remove redundancies and to avoid images with erroneous ground-truth poses. The list of selected images is in the file `/media/compute/vol/ycb/ycb-bop/test_targets_bop19.json`. The selected images are a subset of images listed in "YCB_Video_Dataset/image_sets/keyframe.txt" in the original dataset.
- The 3D models were converted from meters to millimeters and the centers of their 3D bounding boxes were aligned with the origin of the model coordinate system. This transformation was reflected also in the ground-truth poses. The models were transformed so they follow the same conventions as models from other datasets included in BOP and are thus compatible with the BOP toolkit.
- We additionally provide 50K PBR training images generated for the BOP Challenge 2020.

The 80K synthetic training images included in the original version of the dataset are also provided.