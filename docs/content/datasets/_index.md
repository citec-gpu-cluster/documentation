+++
title = "Datasets"
weight = 50
chapter = false
+++

List of datasets currently stored on the compute volume

### How to provide a dataset?

1. request a directory for the compute volume
2. put the dataset there
3. set read only
4. document here

### Listing

{{% children style="h2" description="true" showhidden="true" %}}