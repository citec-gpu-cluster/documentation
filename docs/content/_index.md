---
title: Home Page
date: 2023-10-23T12:46:07+02:00
---

# TechFak GPU Cluster

The GPU compute cluster offers several hybrid nodes with a balanced CPU, RAM, and GPU ratio. Access may be granted upon request by additional user permissions. The cluster utilizes the workload manager [Slurm](https://slurm.schedmd.com/). You may start your jobs via the login node `login-1.gpu.cit-ec.net`. For compute tasks it is mandatory to use the Slurm scheduler.

The cluster nodes run on locally installed Ubuntu 22.04. Due to performance issues, the cluster has a separate network file system. Thus the volumes `/homes` and `/vol` are not the same folders as on the regular TechFak [Netboot](https://techfak.net/netboot) systems, e. g. compute.techfak.de or the workstations. Therefore on your initial login, you find an empty home folder under `/homes/juser`.

If you have not worked with Slurm, please read our instruction [First Steps](getting-started/first-steps).

## Something missing on this page?

If you have any ideas or you are missing some information on this page, please just create a pull request on the [Gitlab](https://gitlab.ub.uni-bielefeld.de/citec-gpu-cluster/documentation) repository.

## Support

On the [Universities Matrix Service](https://matrix.uni-bielefeld.de/) join the Channel `#citec-gpu:uni-bielefeld.de` or send a support request to [GPU Support](mailto:gpu-support@techfak.de)


