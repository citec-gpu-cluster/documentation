---
title: Getting Started
date: 2023-10-23T13:03:06+02:00
weight: 5
chapter: true
pre: <b>1. </b>
---

# Getting Started

If you did not have worked with Slurm, we recommend following the instructions in the [first steps](first-steps). You will first log on the login node, create a new Python environment (including installation of `miniconda`), write an Slurm script to start a job, and learn basic Slurm commands to manage your jobs.