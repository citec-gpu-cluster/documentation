---
title: Examples
date: 2023-10-23T13:03:06+02:00
weight: 10
chapter: true
pre: <b>2. </b>
---

# Find minimal code examples to test your environments

1. How to set up a Conda environment to use [pytorch](torch)?
2. How to use [local storage](localstorge) on the compute node to speed up the Slurm jobs?